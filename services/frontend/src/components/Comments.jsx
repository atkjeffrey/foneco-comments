import Grid from "@material-ui/core/Grid";
import {Paper} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import React from "react";
import styles from "../style.css"
import Box from "@material-ui/core/Box";

const Comments = (props) => {
    return <Grid
        container
        direction="column-reverse"
        justify={"flex-end"}
        alignItems="stretch"
        spacing={2}
    >
        {props.comments.map(comment => {
            return <Grid item component={Paper} style={{padding: "3px", height: "100%"}}>
                <Grid container direction="row" alignItems="center" justify="flex-start">
                    <Grid item xs={12}>
                        <Grid container direction="column" justify="flex-start" alignItems="flex-start">
                            <Grid item>
                                <a href={`mailto:${comment.email}`}>
                                    <Typography variant="h6">{comment.name}</Typography>
                                </a>
                            </Grid>
                            <Grid item>
                                <Typography color="textSecondary" gutterBottom>
                                    {new Date(comment.submitted_on * 1000).toLocaleDateString("en-US", {
                                        weekday: 'long',
                                        year: 'numeric',
                                        month: 'long',
                                        day: 'numeric'
                                    })}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="body2" component="p" gutterBottom>{comment.comment}</Typography>
                    </Grid>
                </Grid>
            </Grid>
        })}
    </Grid>
};

export default Comments;