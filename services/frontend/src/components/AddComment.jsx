import React from 'react';
import {Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import styles from '../style.css'

const AddComment = (props) => {
    return <form onSubmit={(event) => props.addComment(event)}>
        <Typography variant="h5" gutterBottom>Add a comment</Typography>
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <TextField
                    required
                    id="name"
                    name="name"
                    label="Name"
                    fullWidth
                    autoComplete="name"
                    value={props.name}
                    onChange={props.handleChange}
                />
                <TextField
                    required
                    id="email"
                    name="email"
                    label="Email Address"
                    fullWidth
                    autoComplete="email"
                    value={props.email}
                    onChange={props.handleChange}
                />
                <TextField
                    required
                    fullWidth
                    id="comment"
                    name="comment"
                    label="Comment"
                    multiline={true}
                    value={props.comment}
                    onChange={props.handleChange}
                />
            </Grid>
            <Button
                variant="contained"
                color="primary"
                type="submit">
                Submit
            </Button>
        </Grid>
    </form>
};

export default AddComment;
