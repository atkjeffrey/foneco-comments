import React, {Component} from 'react';  // new
import ReactDOM from 'react-dom';
import axios from 'axios';
import Comments from "./components/Comments";
import {Typography} from "@material-ui/core";
import AddComment from "./components/AddComment";
import Grid from "@material-ui/core/Grid";
import io from 'socket.io-client'
import styles from './style.css'
import Container from "@material-ui/core/Container";

const socket = io(`${process.env.REACT_APP_COMMENTS_SERVICE_URL}/comments`);

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            comments: [],
            name: "",
            email: "",
            comment: ""
        };
        this.addComment = this.addComment.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    setSocketListeners() {
        socket.on('comment_added', (data) => {
            if (data.status === "success") {
                this.setState(previous_state => ({
                    name: "",
                    email: "",
                    comment: "",
                    comments: [...previous_state.comments, data.comment]
                }));
            }
        })
    }

    addComment(event) {
        event.preventDefault();
        const data = {
            name: this.state.name,
            email: this.state.email,
            comment: this.state.comment,
            submitted_on: Date.now() / 1000
        };
        socket.emit('add_comment', data);
    };

    handleChange(event) {
        const obj = {};
        obj[event.target.name] = event.target.value;
        this.setState(obj);
    };

    render() {

        return (
            <Container fixed>
                <Grid
                    container
                    direction="column"
                    justify="flex-start"
                    alignItems="stretch"
                    className={styles.root}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5" gutterBottom>Comments</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Comments comments={this.state.comments} xs={12}/>
                    </Grid>
                    <br/>
                    <Grid item xs={12}>
                        <AddComment name={this.state.name}
                                    email={this.state.email}
                                    comment={this.state.comment}
                                    addComment={this.addComment}
                                    handleChange={this.handleChange}
                        />
                    </Grid>
                </Grid>
            </Container>
        )
    }

    getComments() {
        axios.get(`${process.env.REACT_APP_COMMENTS_SERVICE_URL}/comments`)
            .then((res) => {
                this.setState({comments: res.data.data.comments});
            })
            .catch((err) => {
                console.log(err);
            });
    }

    componentDidMount() {
        this.getComments();
        this.setSocketListeners();
    };
}

ReactDOM.render(
    <App/>,
    document.getElementById('root')
);