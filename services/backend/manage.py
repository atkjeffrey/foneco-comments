import sys
import unittest

from flask.cli import FlaskGroup

from src import create_app, db, socketio

app = create_app()  # new

cli = FlaskGroup(create_app=create_app)  # new

@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command()
def test():
    """Runs the tests without code coverage"""
    tests = unittest.TestLoader().discover('src/tests')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    sys.exit(result)


# @cli.command('run_socket')
# def run_socket():
#     socket.run(create_app(), host="0.0.0.0", port=5000)

if __name__ == '__main__':
    cli()
