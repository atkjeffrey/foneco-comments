#!/bin/sh

echo "Waiting for MySQL..."

while ! mysqladmin ping -h"database" --silent; do
    sleep 1
done

echo "MySQL started"

python manage.py recreate_db
python manage.py run -h 0.0.0.0