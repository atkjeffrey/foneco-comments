import json
import time
import unittest
from datetime import datetime

from src import db
from src.api.models import Comment
from src.tests.base import BaseTestCase


def add_comment(name, email, text, submitted_on):
    comment = Comment(name, email, text, submitted_on)
    db.session.add(comment)
    db.session.commit()
    return comment

class TestCommentsService(BaseTestCase):

    def test_get_comments(self):
        submitted_on_john = datetime.utcnow().timestamp()
        comment_john = add_comment(name='John', email='john@3050feralhogs.com', text='unhhhh', submitted_on=submitted_on_john)
        submitted_on_jane = datetime.utcnow().timestamp() + 2
        comment_jane = add_comment(name='Jane', email='jane@3050feralhogs.com', text='nope', submitted_on=submitted_on_jane)
        submitted_on_bob = datetime.utcnow().timestamp() + 50
        comment_bob = add_comment(name='Bob', email='bob@3050feralhogs.com', text='eh', submitted_on=submitted_on_bob)
        with self.client:
            response = self.client.get(f'/comments')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(data['data']['comments']), 3)
            self.assertIn(comment_john.name, data['data']['comments'][0]['name'])
            self.assertIn(comment_john.email, data['data']['comments'][0]['email'])
            self.assertIn(comment_john.text, data['data']['comments'][0]['text'])
            self.assertEqual(comment_john.submitted_on, data['data']['comments'][0]['submitted_on'])

            self.assertIn(comment_jane.name, data['data']['comments'][1]['name'])
            self.assertIn(comment_jane.email, data['data']['comments'][1]['email'])
            self.assertIn(comment_jane.text, data['data']['comments'][1]['text'])
            self.assertEqual(comment_jane.submitted_on, data['data']['comments'][1]['submitted_on'])

            self.assertIn(comment_bob.name, data['data']['comments'][2]['name'])
            self.assertIn(comment_bob.email, data['data']['comments'][2]['email'])
            self.assertIn(comment_bob.text, data['data']['comments'][2]['text'])
            self.assertEqual(comment_bob.submitted_on, data['data']['comments'][2]['submitted_on'])

if __name__ == '__main__':
    unittest.main()
