from datetime import datetime

from src import db


class Comment(db.Model):
    __tablename__ = 'comments'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(256), nullable=False)
    email = db.Column(db.String(256), nullable=False)
    text = db.Column(db.Text(), nullable=False)
    submitted_on = db.Column(db.Float(precision=32), nullable=False)

    def __init__(self, name, email, text, submitted_on):
        self.name = name
        self.email = email
        self.text = text
        self.submitted_on = submitted_on

    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email,
            'comment': self.text,
            'submitted_on': self.submitted_on
        }
