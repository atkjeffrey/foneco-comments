import sys
from datetime import datetime, time

from flask_socketio import emit
from sqlalchemy import exc

from src import db
from flask import Blueprint, request
from flask_restful import Resource, Api

from src.api import models
from .. import socketio

comments_blueprint = Blueprint('comments', __name__)
api = Api(comments_blueprint)


@socketio.on('add_comment', namespace='/comments')
def handle_comment(comment):
    response_object = {
        'status': 'fail'
    }
    if not comment:
        return
    try:
        name = comment.get('name')
        email = comment.get('email')
        comment_text = comment.get('comment')
        submitted_on = comment.get('submitted_on')
        db.session.add(models.Comment(name=name, email=email, text=comment_text, submitted_on=submitted_on))
        db.session.commit()
        response_object['status'] = 'success'
        response_object['comment'] = comment
        emit('comment_added', response_object, namespace="/comments", broadcast=True)
    except (exc.IntegrityError, exc.OperationalError, TypeError) as e:
        db.session.rollback()


class Comments(Resource):

    def get(self):
        response_object = {
            'status': 'success',
            'data': {
                'comments': [comment.to_json() for comment in models.Comment.query.all()]
            }
        }
        return response_object, 200


api.add_resource(Comments, '/comments')
