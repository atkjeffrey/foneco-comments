import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_socketio import SocketIO

db = SQLAlchemy()
cors = CORS()
socketio = SocketIO()


def create_app(script_info=None):

    app = Flask(__name__)

    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(app_settings)

    db.init_app(app)
    cors.init_app(app)

    from src.api.comments import comments_blueprint
    app.register_blueprint(comments_blueprint)

    socketio.init_app(app)

    @app.shell_context_processor
    def ctx():
        return {'app': app, 'db': db}

    return app
